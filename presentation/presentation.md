---
title: "Creating Mobile Interactive Ephemeral Immersive Installations using FLOSS"
author: Michał Seta
date: Lübeck, November 21, 2022
theme: night
format: revealjs
...

<div class="author">
<img src="img/logo-SAT-full.png" alt="Michał Seta"/>
<div class="name">
<a href="mailto:mseta@sat.qc.ca" title="Michał Seta's Email">mseta@sat.qc.ca</a></br>
<a href="http://djiamnot.xyz" title="Michał Seta's Website">djiamnot.xyz</a>
</div>
</div>

# Who are we?

## Where from?

![](img/map-SAT.png)

## SAT

![](img/logo-SAT-full.png){width=40%}

(Society for Arts and Technology)

https://sat.qc.ca

## SAT

![](img/exterieur_sat_nb_.png)

## Satosphere

![](img/SAT-Satosphere.png)

## Satosphere

![](img/recherche-artistique3.jpg)

## Satosphere

![](img/recherche-artistique4.jpg)

## Satosphere

![](img/recherche-artistique5.jpg)

## FoodLab

![](img/SAT-Foodlab.png)

## Rooftop terrace

![](img/SAT-Terrasse.jpg)

## Espace SAT

![](img/album_louer-espacesespacesat_12.png)

## Espace SAT

![](img/album_louer-espaces_espacesat_6.png)

## Espace SAT

![](img/album_louer-espacesespacesat_9.png)

## Campus

![](img/SAT-Campus.png)

## Café SAT

![](img/cafe-sat-montreal-06.jpg)

## Metalab

![](img/SAT-Metalab.png)

# Innovation at the SAT

## Research Education Pre-commercialisation

![](img/SAT-innovation.png)

## Software made at SAT

![](img/SAT-toolbox.png)

# Collective immersion

## What makes collective immersion different from individual immersion?

# Collective prototyping

## Bonus: Collective prototyping _in-situ_

::: incremental
- a way to boost creativity
- combine skills
:::

## Basic principles for facilitating prototyping

- Using familiar tools/software to minimize learning curve induced friction
- Using strategies that allow for combining the capabilities of all involved software and expertise

## How does collective prototyping stand out?

- Each creator bring their own preferred tool
- Creators are not limited to what a single tool can do
- Possible to experiment "at first sight"
- Freedom/ease to replace one tool with another

## Examples of cross software/hardware communication

::: incremental

- File formats: audio, video, 3D objects, etc.
- Inter-process communication: Syphon, Spout, shmdata, etc.
- Network communication: NDI, OSC, switcher, etc.
- Hardware communication: audio/video cables!

:::

# Our prototyping workflow

## Metalab tools cover

![](img/Our_workflow.png)

::: incremental

- Projection mapping
- Audio spatialization
- Real time control: (could be pose detection)

:::

## The tools

- SATIE
- LivePose
- Splash

# Enhance!

## SATIE

* synchronised with 3D engines
* interfaces with many speakers
* simultaneous rendering
* multitude of sound sources

## SATIE - why?

sound object

<iframe src="https://player.vimeo.com/video/398346954?h=d4ab3f4b2a#t=0m37s&autoplay=1" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

## SATIE is for:

* possible sound sources:
  * live input
  * synthesis
* effects

## SATIE - how?

* OSC (Open Sound Control)

![](img/SATIE-ecosystem.png)

## Demo

Here are some examples of SATIE in action:

<iframe src="https://player.vimeo.com/video/616508216?autoplay=1" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

## Projection mapping with Splash

![](img/splash.png)

## About projection mapping

"Projection Mapping uses everyday video projectors, but instead of projecting on a flat screen (...), light is mapped onto any surface (...)"

-- <cite>[http://projection-mapping.org/what-is-projection-mapping/](http://projection-mapping.org/what-is-projection-mapping/)</cite>

## Projecting onto arbitrary surfaces

![](img/realObjectWithVideo.jpg)

## Projecting onto arbitrary surfaces

![](img/realObjectWireframe.jpg)

## Projection onto mobile surface

<iframe src="https://player.vimeo.com/video/268028595" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

## Handling projections overlaps

![](img/semidome_wireframe_no_blending.jpg)


## Interaction with LivePose

![](img/livepose.png){ height=50% }

## Pose estimation

**Goal**: To estimate the pose of a person in an image by locating special body
points (**keypoints**).

![](img/keypoints_and_people.png)

## Pose estimation in live feeds

<iframe src="img/openpose_walk_0.mp4" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

## Extract additional information

* Coordinates on the floor
* Head tracking
* Simple action detection
* WIP: better action detection for complex actions

## Communication with other tools

* OSC messages
* WebSocket
* WIP: libmapper

## Other useful FOSS tools

* [Chataigne](https://benjamin.kuperberg.fr/chataigne/en#home)
* [OSSIA Score](https://ossia.io/)
* [libmapper](https://libmapper.github.io/)
* [webmapper](https://github.com/libmapper/webmapper)
* [OBS](https://obsproject.com/)
* [Godot](https://godotengine.org)
* [Blender](https://www.blender.org/)

# Bring your own software!

##

- NDI
- OSC
- ...

## Brainstorm & collaborate

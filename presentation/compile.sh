set -e

if [ ! -d reveal.js ]
then
    # this is the last version working with pando 2.5 (default on Ubuntu 20.04)
    wget https://github.com/hakimel/reveal.js/archive/3.9.2.tar.gz
    tar -xzvf 3.9.2.tar.gz
    mv reveal.js-3.9.2 reveal.js
fi

pandoc --standalone \
       -t revealjs \
       -V revealjs-url=./reveal.js \
       -o presentation.html \
       --slide-level 2 \
       --css styles.css \
       -V transition='none' \
       presentation.md
